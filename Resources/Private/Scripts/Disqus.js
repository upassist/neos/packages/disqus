var $disqus = $('#disqus_thread'),
	disqus_shortname = $disqus.data('disqus-shortname'),
	disqus_identifier = $disqus.data('disqus-identifier'),
	disqus_title = $disqus.data('disqus-title'),
	disqus_config = function () {
		this.language = $disqus.data('disqus-language');
	};
(function() {
	var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
	(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();